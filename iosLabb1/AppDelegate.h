//
//  AppDelegate.h
//  iosLabb1
//
//  Created by Anders Zetterström on 2015-01-24.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

