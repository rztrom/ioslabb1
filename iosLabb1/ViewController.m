//
//  ViewController.m
//  iosLabb1
//
//  Created by Anders Zetterström on 2015-01-24.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *colorChange;

@end

@implementation ViewController



-(IBAction)ColorChange:(UISwipeGestureRecognizer *)sender {
    if((self.view.backgroundColor==[UIColor whiteColor])){
        self.view.backgroundColor=[UIColor blackColor];
    }
    else{
        self.view.backgroundColor=[UIColor whiteColor];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
